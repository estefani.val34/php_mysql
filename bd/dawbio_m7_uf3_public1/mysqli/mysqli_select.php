<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>mysqli_select</title>
    </head>
    <body>
        <?php
            //show PHP errors (needed when not active on php.ini file)
            ini_set('display_errors', 'On');
            error_reporting(E_ALL | E_STRICT);
        
            $servername="localhost";
            $username="provenusr";
            $password="Provenpass1.";
            $dbname="proven";

            //Connect to database
            //Instance an object of mysqli class. The class mysqli represent a MySQL database connection
            // (same could be done with the alias mysqli_connect)
            
            @$conn=new mysqli($servername, $username, $password, $dbname); //para poder tener accesos a la BD. MIRAR en php.net

                        
            //get connection error: connect_errno for the code and connect_error for the text
            //connect_errno=0 - connection OK
            if ($conn->connect_errno > 0) {//codigo de error 
                printf("<p>Unable to connect to database:</p><p>%s</p>", $conn->connect_error);// texto del error de la BD
            }
            else { // no error 
                printf("<p>Connected successfully</p>");
                // para ejecutar un comando de la BD(select, insert...)
                $sql=<<<SQL
                    SELECT id,title,DATE_FORMAT(pubdate,'%d/%m/%Y') pubdate,content,category 
                        FROM news ORDER BY pubdate DESC;
SQL;
                //query method performs an SQL sentence on the database 
                //Returns FALSE on failure. 
                //For successful SELECT, SHOW, DESCRIBE or EXPLAIN queries will return a mysqli_result object. 
                //For other successful queries will return TRUE. 
                $result=$conn->query($sql); // devuelve los datos on mysql,

                if (!$result) { 
                    printf("<p>There was an error running the query:</p><p>%s</p>", $conn->error);                
                }
                else {  //returns TRUE or mysqli_result object, on succes
                    printf("<p><ul>");
                     
                    //fetch_array method: Fetch a result row as an associative, a numeric array, or both (default)
                    // Returns NULL if there are no more rows. 
                    
                    //hacer un bucle para leer todos los resitros
                    //while($row=$result->fetch_array(MYSQLI_NUM)) { // para acceder a todas las filas, row es un registro de la tabla
                    //while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                    //while($row=$result->fetch_array(MYSQLI_BOTH)) {
                        /*printf("<li>%s > %s > %s > %s</li>", 
                            $row[2], 
                            $row[0], 
                            utf8_encode($row[1]), 
                            utf8_encode($row[4]));
                      } */
                    
                    
                    // mysqli_fetch_assoc method — Fetch a result row as an associative array
                    // ( is the same as method fetch_array(MYSQLI_ASSOC) 
                    while($row=$result->fetch_assoc()) { // mismo que el fetch_array   , te devuelve cada fila de la tabla,                    
                        printf("<li>%s > %s > %s > %s</li>", 
                            $row['pubdate'], // el nombre que tienen los campos en la tabla 
                            $row['id'], 
                            utf8_encode($row['title']), 
                            utf8_encode($row['category']));
                    }
                    
                    printf("</ul></p>");
                }
                
                // Closes database connection
                $conn->close();// cerrar la BD
                printf("<p>Disconnected successfully</p>"); 
            }
        ?>
    </body>
</html>