<?php

require_once 'ConnectDb.class.php';
require_once 'News.class.php';

class NewsDbDAO { // donde estan los metodos que accedes a la BD

    private $connect; // conexión actual ,   

    public function __construct() {
        // http://php.net/manual/fa/function.mysqli-report.php
        mysqli_report(MYSQLI_REPORT_ALL);

        try {
            $dataSource = new ConnectDb(); // aqui es donde se hace la conección
            $this->connect = $dataSource->getConnection();
        } catch (mysqli_sql_exception $e) {
//            printf("<p>Error code:</p><p>%s</p>", $e->getCode());
//            printf("<p>Error message:</p><p>%s</p>", $e->getMessage());
//            printf("<p>Stack trace:</p><p>%s</p>", nl2br($e->getTraceAsString()));
        }
    }

    public function select($id) {
        $news = null;

        $sql = "SELECT * FROM news WHERE id=?;";

        $stmt = $this->connect->prepare($sql);
        $stmt->bind_param("i", $id);
        $success = $stmt->execute(); // devuelve true o false

        if ($success) {
            $result = $stmt->get_result();
            $row = $result->fetch_assoc();
            $news = new News($row['id'], utf8_encode($row['title']),
                    $row['pubdate'], $row['content'], utf8_encode($row['category']));
        }

        return $news;
    }

    // TODO
    //https://www.php.net/manual/es/mysqli-stmt.bind-param.php
    //http://localhost/~estefani/uf3/dawbio_m7_uf3_public/mysqli_class/NewsModel.php  
    public function insert(News $news) {
        //$id, $title, $pubDate, $content, $category
        $sql = <<<SQL
                    INSERT INTO news (title,pubdate,content,category) 
                        VALUES (?,STR_TO_DATE(?,'%d/%m/%Y'),?,?);
SQL;

        $title = $news->getTitle();
        $pubdate = $news->getPubDate();
        $content = $news->getContent();
        $category = $news->getCategory();

        $stmt = $this->connect->prepare($sql);

        if (!$stmt) {
            printf("<p>Error:</p><p>%s</p>", $this->connect->error);
        } else {
            $stmt->bind_param("ssss", $title, $pubdate, $content, $category); // "ssss" ; son los tipos que van a ver en l base de datos : 4 strings 
            $success = $stmt->execute(); // devuelve true o false

            if (!$success) {
                printf("<p>Error:</p><p>%s</p>", $this->connect->error);
            } else {
                printf("<p>New records created successfully: %s</p>", $stmt->affected_rows);
                return $stmt->affected_rows; // si todo va bien devuelve 1 
            }
        }
        return 0;
    }

    // TODO
    public function update(News $news) {
        $sql = <<<SQL
                    UPDATE news SET title=?,content=?,category=? 
                        WHERE ID=?;
SQL;

        $title = "titulo modificado";
        // $pubdate = $news->getPubDate();
        $content = $news->getContent();
        $category = $news->getCategory();
        $id = $news->getId();

        $stmt = $this->connect->prepare($sql);

        if (!$stmt) {
            printf("<p>Error:</p><p>%s</p>", $this->connect->error);
        } else {
            $stmt->bind_param("ssssi", $title, $content, $category, $id);
            $success = $stmt->execute(); // devuelve true o false

            if (!$success) {
                printf("<p>Error:</p><p>%s</p>", $this->connect->error);
            } else {
                printf("<p>Records updated successfully: %s</p>", $stmt->affected_rows);
                return $stmt->affected_rows;
            }
        }
        return 0;
    }

    // TODO
    public function delete($id) {
        
        printf("<p>Voy a borrar la noticia con id = %s</p>", $id);
        
        $sql = <<<SQL
                    DELETE FROM news WHERE id=?;
SQL;

       // $id = "3";

        $stmt = $this->connect->prepare($sql);

        if (!$stmt) {
            printf("<p>Error:</p><p>%s</p>", $this->connect->error);
        } else {
            $stmt->bind_param("i", $id);
            $success = $stmt->execute(); // devuelve true o false

            if (!$success) {
                printf("<p>Error:</p><p>%s</p>", $this->connect->error);
            } else {
                printf("<p>Records deleted successfully: %s</p>", $stmt->affected_rows);
                return $stmt->affected_rows;
            }
        }
        return 0;
    }

}
