<?php

require_once "model/ModelInterface.class.php";
require_once "model/persist/ConnectDb.class.php";

class UserDbDAO implements ModelInterface {

    private static $instance = NULL; // instancia de la clase
    private $connect; // conexión actual

    public function __construct() {
        $this->connect = (new ConnectDb())->getConnection();
    }

// singleton: patrón de diseño que crea una instancia única
// para proporcionar un punto global de acceso y controlar
// el acceso único a los recursos físicos
    public static function getInstance(): CategoryDbDAO {
        if (self::$instance == NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function searchByUsername($username) {
        try {

            $sql = <<<SQL
                        SELECT username, password, age, role, active
                            FROM user WHERE username=:username;  
SQL;

            //$id = "1";

            $stmt = $this->connect->prepare($sql);
            $stmt->bindParam(":username", $username, PDO::PARAM_STR); //: para las variables 
            $stmt->execute(); // la consulta con la base  de datos 

            if ($stmt->rowCount()) {// devuelve el numero de filas que ha leido 
                $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'User'); // los instance en la clase user
                print_r($stmt->fetch());
                return $stmt->fetch(); // los devuelves 
                //return TRUE;
            }
            return NULL;
        } catch (PDOException $ex) {
            return FALSE;
        }
    }

    public function add($user): bool {
        
    }

    public function modify($user): bool {
        
    }

    public function delete($id): bool {
        
    }

    public function listAll(): array {
        
    }

    public function searchById($id) {
        
    }

}
